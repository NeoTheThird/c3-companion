# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the c3companion.neothethird package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: c3companion.neothethird\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-12-27 05:40+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Home.qml:14
msgid "XXth Chaos Communication Congress"
msgstr ""

#: ../qml/Home.qml:21 ../qml/Settings.qml:14 ../qml/Settings.qml:72
msgid "Settings"
msgstr ""

#: ../qml/Home.qml:26
msgid "Wiki"
msgstr ""

#: ../qml/Home.qml:31
msgid "Open in uNav"
msgstr ""

#: ../qml/Home.qml:65
msgid "XXc3 Companion"
msgstr ""

#: ../qml/Home.qml:84
msgid "Welcome to XXc3!"
msgstr ""

#: ../qml/Settings.qml:42 c3companion.neothethird.desktop.in.h:1
msgid "XXc3 companion"
msgstr ""

#: ../qml/Settings.qml:61
msgid "Version: "
msgstr ""

#: ../qml/Settings.qml:79
msgid "Here be dragons"
msgstr ""

#: ../qml/Settings.qml:90
msgid "About"
msgstr ""

#. TRANSLATORS: Please make sure the URLs are correct
#: ../qml/Settings.qml:100
msgid ""
"This program is free software: you can redistribute it and/or modify it "
"under the terms of the GNU General Public License as published by the Free "
"Software Foundation, either version 3 of the License, or (at your option) "
"any later version. This program is distributed in the hope that it will be "
"useful, but WITHOUT ANY WARRANTY; without even the implied warranty of "
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the <a "
"href='https://www.gnu.org/licenses/gpl-3.0.en.html'>GNU General Public "
"License</a> for more details."
msgstr ""

#: ../qml/Settings.qml:109
msgid "SOURCE"
msgstr ""

#: ../qml/Settings.qml:109
msgid "ISSUES"
msgstr ""

#: ../qml/Settings.qml:109
msgid "DONATE"
msgstr ""
