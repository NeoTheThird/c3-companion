![c3 companion](assets/logo.svg)

# c3 companion for Ubuntu Touch

The [Chaos Communication Congress](https://events.ccc.de/congress/2018/wiki/index.php/Main_Page) is a conference for technology society and utopia. This is an unofficial companion app for Ubuntu Touch.

## How to build

Set up [clickable](https://github.com/bhdouglass/clickable) and run `clickable` inside the working directory.

## Legal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
