// Map.qml
// This file is part of the c3companion App
// Copyright (c) 2018 Jan Sprinz <jan@ubports.com>
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import Qt.labs.settings 1.0

ApplicationWindow {
    id: mainWindow
    title: "36c3 Companion"
    visible: true
    x: screen.desktopAvailableWidth / 4
    y: screen.desktopAvailableHeight / 5
    width: screen.desktopAvailableWidth / 2
    height: screen.desktopAvailableHeight / 1.5
    property variant favourites: []

    Settings {
        category: "Window"
        property alias x: mainWindow.x
        property alias y: mainWindow.y
        property alias height: mainWindow.height
        property alias width: mainWindow.width
    }

    PageStack {
        id: stack
        anchors.fill: parent
    }

    Component.onCompleted: {
        stack.push(Qt.resolvedUrl("Home.qml"));
    }

    function open_detail(_name, _url) {
        console.log("Opening " + _name + " on url " + _url)
        stack.push(Qt.resolvedUrl("Detail.qml"), {
            name: _name,
            url: _url
        });
    }
}
