// Mensa.qml
// This file is part of the c3companion App
// Copyright (c) 2018 Jan Sprinz <jan@ubports.com>
// See Main.qml and LICENSE for more information

import QtQuick 2.9
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import QtWebEngine 1.0

Page {
    id: detailPage

    property string name
    property string url

    header: PageHeader {
        id: header
        title: name
    }

    WebEngineView {
        url: parent.url
        height: parent.height - header.height
        width: parent.width
        anchors.top: header.bottom
    }
}
