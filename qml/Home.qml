// Home.qml
// This file is part of the c3companion App
// Copyright (c) 2018 Jan Sprinz <jan@ubports.com>
// See Main.qml and LICENSE for more information

import QtQuick 2.9
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3

Page {
    id: mainPage

    header: PageHeader {
        title: i18n.tr("XXth Chaos Communication Congress")
        // flickable: scrollView.flickableItem

        trailingActionBar {
            numberOfSlots: 0
            actions: [
                Action {
                    text: i18n.tr("Settings")
                    onTriggered: stack.push(Qt.resolvedUrl("Settings.qml"))
                    iconName: "settings"
                },
                Action {
                    text: i18n.tr("Wiki")
                    onTriggered: Qt.openUrlExternally("https://events.ccc.de/congress/2018/wiki/Main_Page")
                    iconName: "stock_website"
                },
                Action {
                    text: i18n.tr("Open in uNav")
                    onTriggered: Qt.openUrlExternally("http://map.unav.me?51.39417,12.40052")
                    iconName: "location"
                }
            ]
        }
    }

    ScrollView {
        id: scrollView
        anchors {
            top: mainPage.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }

        clip: true

        Column {
            id: aboutColumn
            spacing: units.gu(2)
            width: scrollView.width

            Rectangle {
                height: units.gu(6)
                width: parent.width
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("XXc3 Companion")
                fontSize: "x-large"
            }

            UbuntuShape {
                width: units.gu(24)
                height: width
                anchors.horizontalCenter: parent.horizontalCenter
                radius: "medium"
                image: Image {
                    source: Qt.resolvedUrl("../assets/logo.png")
                }
            }

            Label {
                width: parent.width
                linkColor: UbuntuColors.orange
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("Welcome to XXc3!")
            }

            Button {
                text: "Fahrplan"
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: open_detail(text, "https://fahrplan.events.ccc.de/congress/2019/Fahrplan/timeline.html")
            }

            Button {
                text: "Livestreams"
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: open_detail(text, "http://streaming.media.ccc.de/36c3")
            }

            Button {
                text: "Map"
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: open_detail(text, "https://36c3.c3nav.de/")
            }
        }
    }
}
